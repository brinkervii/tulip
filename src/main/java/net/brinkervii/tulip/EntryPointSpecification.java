package net.brinkervii.tulip;

import lombok.Data;

import java.util.regex.Pattern;

@Data
public class EntryPointSpecification {
	private final static Pattern ENTRY_POINT_PATTERN = Pattern.compile("---\\s*@entrypoint\\s+(.+?)\\s+(.*)");
	private final String type;
	private final String location;

	private EntryPointSpecification(String type, String location) {
		this.type = type.trim();
		this.location = location.trim();
	}

	public static boolean isEntryPoint(String line) {
		return ENTRY_POINT_PATTERN.matcher(line).matches();
	}

	public static EntryPointSpecification fromLine(String line) {
		final var matcher = ENTRY_POINT_PATTERN.matcher(line);
		if (matcher.find()) {
			return new EntryPointSpecification(
					matcher.group(1),
					matcher.group(2)
			);
		}

		return null;
	}
}
