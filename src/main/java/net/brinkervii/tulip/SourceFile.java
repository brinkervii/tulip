package net.brinkervii.tulip;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class SourceFile {
	private final static Pattern REQUIRE_AND_ASSIGN_PATTERN = Pattern.compile("local (.+?)\\s*=\\s*require\\(\"(.+)?\"\\)(.*)");
	private final static Pattern REQUIRE_PATTERN = Pattern.compile("require\\s*\\(\\s*[\"'](.+)?['\"]\\s*\\)(.*)");
	private final static Pattern NAME_PATTERN = Pattern.compile(String.format(".*\\%s(.*)\\.[Ll][Uu][Aa]$", File.separator));

	private SourceBundle bundle;
	@Getter
	private boolean entryPoint = false;
	@Getter
	private EntryPointSpecification entryPointSpecification = null;

	@Getter
	private final String id = UUID.randomUUID().toString();
	private final File file;
	@Getter
	private final File relativeFile;
	@Getter
	private final String moduleName;
	private final List<SourceDependency> dependencies;

	public SourceFile(SourceBundle bundle, File file) {
		this.bundle = bundle;
		this.file = file.toPath().toAbsolutePath().normalize().toFile();
		this.relativeFile = FileUtil.relativize(bundle.getRoot(), file);
		this.moduleName = makeModuleName();
		this.dependencies = readDependencies();
	}

	private String makeModuleName() {
		final StringBuilder stringBuilder = new StringBuilder();
		for (String s : this.relativeFile.toString().replaceAll(".[lL][uU][aA]$", "").split(File.separator)) {
			if (stringBuilder.length() > 0)
				stringBuilder.append('.');

			stringBuilder.append(s);
		}

		return stringBuilder.toString();
	}

	private List<SourceDependency> readDependencies() {
		try {
			return Files.readAllLines(file.toPath(), StandardCharsets.UTF_8).stream()
					.map(line -> {
						if (EntryPointSpecification.isEntryPoint(line)) {
							this.entryPoint = true;
							this.entryPointSpecification = EntryPointSpecification.fromLine(line);
						}

						return REQUIRE_PATTERN.matcher(line);
					})
					.filter(Matcher::find)
					.map(matcher -> new SourceDependency(matcher.group(1), matcher.group(2)))
					.collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}

		return new LinkedList<>();
	}

	public String name() {
		final Matcher matcher = NAME_PATTERN.matcher(file.toString());
		if (matcher.find()) {
			return matcher.group(1);
		}

		return "__could_not_find_name__";
	}

	public File outputFile(File outputRoot) {
		if (entryPoint) {
			return new File(outputRoot, entryPointSpecification.getLocation().replace(".", File.separator));
		} else {
			return new File(outputRoot, relativeFile.toString());
		}
	}

	public Stream<SourceDependency> streamDependencies() {
		return dependencies.stream();
	}

	public Iterable<String> transform(HashMap<String, SourceFile> reference) throws IOException {
		final AtomicBoolean requireZiptie = new AtomicBoolean(false);
		final List<String> lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8).stream()
				.map(line -> {
					final Matcher matcher = REQUIRE_PATTERN.matcher(line);
					if (matcher.find()) {
						final String requireString = matcher.group(1);
						final String trailer = matcher.group(2);

						if (reference.containsKey(requireString)) {
							final SourceFile sourceFile = reference.get(requireString);

							requireZiptie.set(true);

							return line.replace(matcher.group(0), String.format("ziptie:Require(\"%s\")%s", requireString, trailer));
						}
					}

					return line;
				}).collect(Collectors.toList());

		if (requireZiptie.get()) {
			if (isEntryPoint()) {
				// Bootstrapper
				final String modulesFolderName = name() + "_Modules";
				lines.add(0, "ziptie:SourcesRoot(modulesFolder)");
				lines.add(0, "local ziptie = require(modulesFolder:WaitForChild(\"Ziptie\"))");
				lines.add(0, String.format("local modulesFolder = script.Parent:WaitForChild(\"%s\")", modulesFolderName));
			} else {
				lines.add(0, "local ziptie = require(script.Parent:WaitForChild(\"Ziptie\"))");
			}
		}

		return lines;
	}
}
