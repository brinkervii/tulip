package net.brinkervii.tulip;

import lombok.Getter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SourceBundle extends ArrayList<File> {
	@Getter
	private final File root;

	public SourceBundle(File root) {
		this.root = root;
	}

	public List<SourceFile> sourceFiles() {
		return this.stream()
			.map(file -> new SourceFile(this, file))
			.collect(Collectors.toList());
	}
}
