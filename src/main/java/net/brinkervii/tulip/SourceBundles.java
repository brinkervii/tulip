package net.brinkervii.tulip;

import java.util.ArrayList;
import java.util.List;

public class SourceBundles extends ArrayList<SourceBundle> {
	public List<SourceFile> getAllFiles() {
		final ArrayList<SourceFile> files = new ArrayList<>();
		this.forEach(bundle -> files.addAll(bundle.sourceFiles()));
		return files;
	}
}
