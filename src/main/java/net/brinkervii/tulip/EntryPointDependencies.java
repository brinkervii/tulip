package net.brinkervii.tulip;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@Slf4j
public class EntryPointDependencies {
	@Getter
	private final SourceFile entryPoint;
	private final Map<String, SourceFile> sourceReference;
	private final HashMap<String, SourceFile> dependencies = new HashMap<>();

	public EntryPointDependencies(Map<String, SourceFile> sourceReference, SourceFile entryPoint) {
		this.sourceReference = sourceReference;
		this.entryPoint = entryPoint;

		entryPoint.streamDependencies().forEach(dep -> this.addDependency(entryPoint, dep));
	}

	private void addDependency(SourceFile parent, SourceDependency dependency) {
		final String requireString = dependency.getRequireString();

		if (!sourceReference.containsKey(requireString)) {
			log.warn(String.format("Dependency %s required for module %s was not found", requireString, parent.getModuleName()));
			return;
		}

		final SourceFile dependencyFile = sourceReference.get(requireString);
		dependencies.put(requireString, dependencyFile);

		dependencyFile.streamDependencies().forEach(dep -> this.addDependency(dependencyFile, dep));
	}

	public Stream<SourceFile> stream() {
		return dependencies.values().stream();
	}
}
