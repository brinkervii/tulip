package net.brinkervii.tulip;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.ArrayList;

@JsonSerialize
@Data
public class TulipProjectConfiguration {
	private ArrayList<String> sourcesRoot = new ArrayList<>();
}
