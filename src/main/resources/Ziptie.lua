---@type RunService
local runService = game:GetService("RunService")

local ziptie = {
	Modules = {},
	RequireCache = {},
	---@type Folder
	JumbleContainer = Instance.new("Folder")
}

function ziptie:SourcesRoot(dir)
	for _, module in pairs(dir:GetDescendants()) do
		if module:IsA("ModuleScript") and module.Name ~= script.Name then
			table.insert(self.Modules, module)
		end
	end
end

--- Jumble up a lua source container
---@param sourceContainer LuaSourceContainer
function ziptie:NukeContainer(sourceContainer)
	if runService:IsStudio() then
		-- Do nothing, we want to keep scripts in studio
		
	else
		sourceContainer.Name = "?"
		sourceContainer.Parent = self.JumbleContainer
		self.JumbleContainer.Name = tostring(math.random(1, 666))
	end
end

function ziptie:Require(moduleName)
	if type(moduleName) == "table" then
		local out = {}

		for _, v in pairs(moduleName) do
			table.insert(out, self:Require(v))
		end

		return unpack(out)
	end

	assert(type(moduleName) == "string")

	if self.RequireCache[moduleName] then
		return self.RequireCache[moduleName]
	end

	for _, module in pairs(self.Modules) do
		if module.Name == moduleName then
			local loaded;
			local success, msg = pcall(function()
				loaded = require(module)
			end)

			if not success then
				error("Module " .. module.Name .. " failed to load properly.\n" .. tostring(msg))
				return
			end

			self.RequireCache[moduleName] = loaded

			if type(loaded) == "table" then
				local init = loaded.Init
				if type(init) == "function" then
					init(loaded, self)
				end
			end

			if runService:IsClient() then
				self:NukeContainer(module)
			end

			wait()
			return loaded
		end
	end

	error(string.format("The module with name '%s' could not be required.", moduleName))
end

function ziptie:Boot(bootModules)
	for _, module in pairs(bootModules) do
		self:Require(module)
	end
end

return ziptie
